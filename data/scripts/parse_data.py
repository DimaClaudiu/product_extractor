import pandas as pd

# Filtering the flipcart dataset
df = pd.read_csv('../datasets/flipkart.csv')
df.dropna()

with open('my_others.txt', 'r', encoding='utf-8') as f:
    my_others = f.readlines()

with open('my_products.txt', 'r', encoding='utf-8') as f:
    my_products = f.readlines()


others = []
products = []
prices = []

# Filtering my scrapped data
for x in my_products:
    if '$' not in x and '– Page' not in x:
        products.append(x.replace('\n', ''))


for x in my_others:
    if '$' not in x:
        others.append(x.replace('\n', ''))

for _, row in df.iterrows():
    if 'Home' in row['product_category_tree'] or 'Furniture' in row['product_category_tree']:
        products.append(row['product_name'])
        prices.append(row['retail_price'])
        others.append(row['product_category_tree'])
        others.append(row['crawl_timestamp'])


# Filtering the ikea dataset
df2 = pd.read_csv('../datasets/ikea.csv')

ikea = []
for _, row in df2.iterrows():
    if 'section' not in row['short_description']:
        ikea.append(row['short_description'].split(',')[0])
        prices.append('$' + str(row['price']))

# Removing duplicates
ikea = list(dict.fromkeys(ikea))
prices = list(dict.fromkeys(prices))
others = list(dict.fromkeys(others))

# Assuring everything is string
others = [str(x) for x in others]
prices = [str(x) for x in prices]


# Removing whitespaces
products += ikea
products = [x.strip() for x in products]

# Creating labels
product_labels = [0] * len(products)
others_labels = [1] * len(others)
prices_labels = [2] * len(prices)

# Wrapping up dataset
text = products + others + prices
labels = product_labels + others_labels + prices_labels
labels = [str(x) for x in labels]

with open('../texts.txt', 'w', encoding='utf-8') as f:
    f.write('\n'.join(text))


with open('../labels.txt', 'w', encoding='utf-8') as f:
    f.write('\n'.join(labels))
