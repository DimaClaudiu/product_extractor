
from urllib.request import urlopen
from bs4 import BeautifulSoup


def list_duplicates(seq):
    seen = set()
    seen_add = seen.add

    seen_twice = set(x for x in seq if x in seen or seen_add(x))

    return list(seen_twice)


all_products = []

for i in range(31):
    url = 'https://www.factorybuys.com.au/collections/furniture?page=' + \
        str(i+1)
    html = urlopen(url).read()
    soup = BeautifulSoup(html, features="html.parser")

    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()

    text = soup.get_text()

    lines = (line.strip() for line in text.splitlines())

    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))

    # drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)

    # Some lines are repeated sequentially, we don't want those
    last_line = ''
    for line in text.split('\n'):
        # Product names usually aren't too short or too long, a good first heuristic
        if len(line) > 5 and len(line) < 256:
            if last_line != line:
                all_products.append(line)
        last_line = line


dup = list_duplicates(all_products)
with open('my_others.txt', 'a', encoding='utf-8') as f:
    f.write('\n'.join(dup))

with open('my_products.txt', 'a', encoding='utf-8') as f:
    for prod in all_products:
        if prod not in dup and '$' not in prod:
            f.write(prod + '\n')
