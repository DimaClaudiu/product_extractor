from urllib.request import urlopen
from bs4 import BeautifulSoup
from classifier.TfClassifier import TfClassifier
from urllib.error import HTTPError


with open('../data/furniture_stores_pages.csv', 'r', encoding='utf-8') as f:
    urls = f.read().splitlines()

model = TfClassifier()
model.load('./tf_classifier.h5')


for url in urls:
    try:
        html = urlopen(url).read()
        soup = BeautifulSoup(html, features="html.parser")

        # kill all script and style elements
        for script in soup(["script", "style"]):
            script.extract()

        text = soup.get_text()

        lines = (line.strip() for line in text.splitlines())

        # break multi-headlines into a line each
        chunks = (phrase.strip()
                  for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)

        print(url)
        with open('extracted_products2.txt', 'a', encoding='utf-8') as f:
            f.write(f'\n{url}\n')
            for line in text.split('\n'):
                if len(line) > 5 and len(line) < 256:
                    label = model.predict(line)
                    if label == 0:
                        f.write(f'{line}\n')
    except HTTPError:
        print(f'Not Found: {url}')
