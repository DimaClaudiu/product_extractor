from classifier.TfClassifier import TfClassifier
import pandas as pd
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical


def train_model(texts, labels, epochs=3, batch_size=20, save_path='./tf_classifier.h5'):

    model = TfClassifier()
    model.create(num_classes=len(set(labels)))

    x_train = texts.to_list()
    y_train = to_categorical(labels)

    model.train(x_train, y_train, save_path, epochs=epochs,
                batch_size=batch_size)


def eval_model(model_path, texts, labels):

    model = TfClassifier()
    model.load(model_path)

    x_test = texts.to_list()
    y_test = to_categorical(labels)

    eval_results = model.eval(x_test, y_test)

    print(eval_results)


if __name__ == '__main__':

    with open('../data/texts.txt', 'r', encoding='utf-8') as f:
        texts = f.read().splitlines()

    with open('../data/labels.txt', 'r', encoding='utf-8') as f:
        labels = f.read().splitlines()

    print(len(texts))
    print(len(labels))

    data = {'text': texts,
            'labels': labels}

    df = pd.DataFrame(data=data)
    df = df.dropna()
    data, data_test = train_test_split(
        df, test_size=0.2)

    train_model(data['text'], data['labels'], epochs=1)

    eval_model('./tf_classifier.h5', data_test['text'], data_test['labels'])
