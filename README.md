# product_extractor

Extract just the products from any e-commerce site.

## Intro
The problem is framed as a Multi-Label Classification problem, where the input is a text sequence, and the output is an integer representation of a class.

I fine-tunned a BERT-base model, trained on two e-commerce datasets, and on scrapped by myself.

## Requirements 
```
tensorflow-gpu==2.3.0
transformers==3.1.0
scikit-learn==0.23.2
pandas==1.1.2
beautifulsoup4==4.9.2
```


## Usage
Scrape and parse data:
```
python data/scripts/web_scrape.py
python data/scripts/parse_data.py
```

Train Model:
```
python train.py
```

Do inference:
```
python product_extractor.py
```

## Classes
**{<br>
'PRODUCT': 0 <br>
'PRICE': 1<br>
'OTHERS': 2<br>}**
